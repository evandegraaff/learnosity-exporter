<?php

namespace App;

/**
 * Class Exporter
 */
class Exporter
{
    /**
     * @var Requester
     */
    private $requester;

    /**
     * Exporter constructor.
     * @param Requester $requester
     */
    public function __construct(
        Requester $requester
    ) {
        $this->requester = $requester;
    }

    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportItems(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/itembank/items', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            $filename = $dataDir.'/'.$this->sanitizeFilename($item['reference']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }

    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportQuestions(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/itembank/questions', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            if (!file_exists($dataDir.'/'.$item['type'])) {
                mkdir($dataDir.'/'.$item['type']);
            }

            $filename = $dataDir.'/'.$this->sanitizeFilename($item['type']).'/'.$this->sanitizeFilename($item['reference']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }
    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportActivities(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/itembank/activities', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            $filename = $dataDir.'/'.$this->sanitizeFilename($item['reference']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }

    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportFeatures(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/itembank/features', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            if (!file_exists($dataDir.'/'.$item['type'])) {
                mkdir($dataDir.'/'.$item['type']);
            }

            $filename = $dataDir.'/'.$this->sanitizeFilename($item['type']).'/'.$this->sanitizeFilename($item['reference']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }

    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportTags(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/itembank/tagging/tags', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            $filename = $dataDir.'/'.$this->sanitizeFilename($item['name']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }

    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportPools(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/itembank/pools', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            $filename = $dataDir.'/'.$this->sanitizeFilename($item['reference']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }

    /**
     * @param string      $dataDir
     * @param string|null $next
     *
     * @return array
     */
    public function exportSessions(string $dataDir, string $next = null)
    {
        $body = $this->requester->get('https://data.learnosity.com/v1/sessions/responses', $next);

        $count = 0;

        foreach ($body['data'] as $item) {
            $filename = $dataDir.'/'.$this->sanitizeFilename($item['session_id']).'.json';

            file_put_contents($filename, json_encode($item));

            ++$count;
        }

        return [$this->getNext($body), $count];
    }

    /**
     * @param array $body
     *
     * @return bool|string
     */
    private function getNext($body)
    {
        $next = true;

        if (isset($body['meta'], $body['meta']['status'])) {
            if ($body['meta']['status'] === true) {
                if (isset($body['meta']['next'])) {
                    $next = $body['meta']['next'];
                } else {
                    $next = false;
                }
            }
        }

        return $next;
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    private function sanitizeFilename(string $filename)
    {
        return preg_replace('#[^a-zA-Z0-9-\._]+#', '_', $filename);
    }
}
